@extends('layouts.app')

@section('title', 'Tipo Documentos')

@section('breadcrumb')
<li class="active">
    <strong>tipo documentos</strong>
</li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Lista de Tipos de Documentos </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <span class="pull-right"><a class="btn btn-primary" href="{{ route('tipodocumento.create') }}">Crear Tipo de Documento</a></span>                    
                    <table class="table table-striped table-responsive" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Activo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse ($tipos as $tipo)
                            <tr>
                                <td>{{ $tipo->name }}</td>
                                <td>
                                    <div class="switch">
                                        <form action="{{ route('tipodocumento.update', $tipo) }}" method="POST">
                                            {{ method_field('PUT') }} 
                                            {{ csrf_field() }}
                                            <div class="onoffswitch">
                                                <input type='hidden' value='0' name='estado'>
                                                <input 
                                                    name="estado" 
                                                    value="1" 
                                                    type="checkbox" 
                                                    class="onoffswitch-checkbox" 
                                                    id="estado-{{ $tipo->id }}" 
                                                    onChange="this.form.submit()" 
                                                    {{ $tipo->estado ? 'checked': null }} 
                                                >
                                                <label class="onoffswitch-label" for="estado-{{ $tipo->id }}">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </form>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ route('tipodocumento.edit', $tipo->id) }}" class="text-warning" title="Editar">
                                        <i class="fa fa-pencil-square-o fa-2x"></i>
                                    </a>
                                    <a href="{{ route('tipodocumento.show', $tipo) }}" class="text-warning" title="Ver">
                                        <i class="fa fa-eye fa-2x"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            No hay Usuario
                        @endforelse
                        </tbody>
                    </table>
                    <span class="pull-right">{{ $tipos }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection