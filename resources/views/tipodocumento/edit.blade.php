@extends('layouts.app')

@section('title', 'Editar Tipo Documento')

@section('breadcrumb')
<li>
    <a href="{{ route('tipodocumento.index') }}">tipo documentos</a>
</li>
<li class="active">
    <strong>editar</strong>
</li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-offset-3 col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Creando Tipo Documento <small>todos los capos son requeridos</small></h5>
            </div>
            <div class="ibox-content">
                <form action="{{ route('tipodocumento.update', $tipo) }}" method="POST" class="form-horizontal" role="form">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="name" value="{{ old('name') ?? $tipo->name }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('softland') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Sigla Softland</label>
                        <div class="col-sm-8">
                            <input class="form-control" name="softland" value="{{ old('softland') ?? $tipo->softland }}">
                            @if ($errors->has('softland'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('softland') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('banco_id') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Codigo Banco</label>
                        <div class="col-sm-8">
                            <input class="form-control" name="banco_id" value="{{ old('banco_id') ?? $tipo->banco_id }}">
                            @if ($errors->has('banco_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('banco_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary pull-right">Guardar cambios</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection