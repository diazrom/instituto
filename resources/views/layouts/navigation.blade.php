<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<li class="nav-header">
				<div class="dropdown profile-element">
					<span>
                        <img src="{{ asset('files/irfe.png')}}" width="55" alt="" class="img-circle">
                    </span>
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ Auth::user()->name }}</strong>
                            </span> <span class="text-muted text-xs block">Finanzas<b class="caret"></b></span>
                        </span>
                    </a>
					<ul class="dropdown-menu animated fadeInRight m-t-xs">
						<li>
							<a href="#">
                                    <i class="fa fa-user"></i> Perfil
                            </a>
						</li>
						<li>
							<a href="#">
                                <i class="fa fa-envelope"></i> Mensajes
                            </a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-2').submit();">
                                <i class="fa fa-sign-out"></i> Salir
                            </a>
							<form id="logout-form-2" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
						@impersonating
						<li>
							<a href="{{ route('impersonate.leave') }}">
								<i class="fa fa-user-secret"></i><i class="fa fa-sign-out"></i> Dejar de personificar
							</a>
						</li>
						@endImpersonating
					</ul>
				</div>
				<div class="logo-element">
					HT+
				</div>
			</li>
			<li class="{{ isActiveRoute('home') }}">
				<a href="{{ route('home') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Inicio</span></a>
			</li>
            <li class="{{ isActiveRoute('resources') }}">
                <a href="javascript:void(0);"><i class="fa fa-users"></i><span class="nav-label">Finanzas</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" style="height: 0px;">
                    <li class="isActiveRoute('home')"><a href="{{ route('finanza.index') }}"><i class="fa fa-search"></i>Conciliacion</a></li>
                    <!--<li class="isActiveRoute('home')"><a href="{{ route('roles.index') }}"><i class="fa fa-user-plus"></i>Agregar</a></li>-->
                </ul>
            </li> 
<!--            <li class="{{ isActiveRoute('offer') }}">
                <a href="javascript:void(0);"><i class="fa fa-handshake-o"></i><span class="nav-label">Ofertas Laborales</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" style="height: 0px;">
                    <li class="isActiveRoute('home')"><a href="{{ route('roles.index') }}"><i class="fa fa-search"></i>Buscar</a></li>
                    <li class="isActiveRoute('home')"><a href="{{ route('roles.index') }}"><i class="fa fa-user-plus"></i>Agregar</a></li>
                    <li class="isActiveRoute('home')"><a href="{{ route('roles.index') }}"><i class="fa fa-list"></i>Listar</a></li>
                </ul>
            </li> 
            <li class="{{ isActiveRoute('maintainers') }}">
                <a href="javascript:void(0);"><i class="fa fa-gears"></i><span class="nav-label">Mantenedores</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" style="height: 0px;">
                    <li class="isActiveRoute('home')"><a href="{{ route('roles.index') }}">Competencias</a></li>
                    <li class="isActiveRoute('home')"><a href="{{ route('roles.index') }}">Carreras</a></li>
                    <li class="isActiveRoute('home')"><a href="{{ route('roles.index') }}">Categorias</a></li>
                </ul>
            </li> -->
			<li class="{{ isActiveRoute('roles') }} {{ isActiveRoute('permissions') }} {{ isActiveRoute('users') }} {{ isActiveRoute('logs') }}">
				<a href="javascript:void(0);"><i class="fa fa-laptop"></i><span class="nav-label">Administración</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" style="height: 0px;">
					@if(auth()->user()->hasRole('Admin'))
                    <li class="{{ isActiveRoute('users') }}"><a href="{{ route('users.index') }}" id="damian"><i class="fa fa-users"></i>Usuarios</a></li>
					<li class="{{ isActiveRoute('roles') }}"><a href="{{ route('roles.index') }}"><i class="fa fa-address-card"></i>Roles</a></li>
					<li class="{{ isActiveRoute('permissions') }}"><a href="{{ route('permissions.index') }}"><i class="fa fa-user-secret"></i>Permisos</a></li>
                    <li class="{{ isActiveRoute('logs') }}"><a href="{{ route('logs.index') }}"><i class="fa fa-file-text"></i>Logs</a></li>
                    @endif
                    <li class="{{ isActiveRoute('logs') }}"><a href="{{ route('tipodocumento.index') }}"><i class="fa fa-file-text"></i>Tipos Documentos</a></li>
				</ul>
            </li>               
		</ul>
	</div>
</nav>