<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IRFE - @yield('title') </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />

</head>
<body class="fixed-sidebar">

  <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        @include('layouts.navigation')

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Page wrapper -->
            @include('layouts.topnavbar')

            <!-- Breadcrumb view -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>@yield('title')</h2>
                    <ol class="breadcrumb">
                        <li class="active">
                            <a href="{{ route('home') }}">Inicio</a>
                        </li>
                        @yield('breadcrumb')
                    </ol>
                </div>
            </div>
            <div class="wrapper wrapper-content" style="font-weight: normal; font-size: 12px;">
                <!-- Main view  -->
                @yield('content')
                <flash title="{{ Auth::user()->name }}:" message="{{ session('flash') }}" level="{{ session('type') }}"></flash>

            </div>

            <!-- Footer -->
            @include('layouts.footer')

        </div>
        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->

<script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/main.js') !!}" type="text/javascript"></script>

@section('scripts')
@show

</body>
</html>
