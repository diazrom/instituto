<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>IRFE - @yield('title') </title>
    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
</head>
<body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown" id="app">
        <div id="wrapper">
            @yield('content')
        </div>
    </div>    
    <script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('js/main.js') !!}" type="text/javascript"></script>
</body>
</html>
