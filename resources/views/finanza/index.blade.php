@extends('layouts.app')

@section('title', 'Conciliacion BECH')

@section('breadcrumb')
    <li>
        <a href="">Conciliacion</a>
    </li>
    <li class="active">
        <strong>ver<strong>
    </li>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-offset-3 col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Subir archivo banco</h5>
            </div>
            <div class="ibox-content">
                <form action="{{ route('finanza.cargar') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Nombre</label>
                        <div class="col-sm-8">
                            <input type="file" class="form-control" name="file" value="{{ old('file') }}">
                            @if ($errors->has('file'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary pull-right">Cargar</button>
                        </div>
                    </div>
                </form>
        @if(isset($err))
        Archivo con errores:<br><br>
            @foreach($err as $er)
                No existe el tipo de documento <strong>{{ $er }}</strong><br>
            @endforeach
        <br>Favor agregar nuevo tipo de documento si corresponde.
        @endif
            </div>
        </div>
    </div>
</div>
@endsection
