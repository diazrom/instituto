@extends('layouts.app')

@section('title', 'Cambiar contraseña');

@section('breadcrumb')
    <li>
        <a href="{{ route('users.index') }}">lista de usuarios</a>
    </li>
    <li>
        <strong>cambiar contraseña de {{ $user->name }}</strong>
    </li>
@endsection;

@section('content')
    <div class="row">
        <div class="col-lg-offset-3 col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cambiar contraseña </h5>
                </div> 
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="{{ route('change-password.update', $user) }}" role="form">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
 
                        <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
                            <label for="current_password" class="col-md-4 control-label">Contraseña actual</label>
 
                            <div class="col-md-6">
                                <input id="current_password" type="password" class="form-control" name="current_password" required>
 
                                @if ($errors->has('current_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('current_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Nueva contraseña</label>
 
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
 
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar nueva contraseña</label>
 
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Cambiar contraseña
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection