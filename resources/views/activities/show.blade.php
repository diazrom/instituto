@extends('layouts.app')

@section('title', 'Logs')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                    <i class="fa fa-btn fa-fw fa-file-text-o"></i> Logs
            </div>
            <div class="ibox-content">
                    @forelse ($activities as $activity)
                    <div class="panel panel-default">
                        <div class="panel-body" id="headingOne"  role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $activity->id }}" aria-expanded="false" aria-controls="collapseOne" class=" " >
                            @include ("activities.types.{$activity->event}")
                            <span class="pull-right "> {{ $activity->elapsed_time }} <i class="fa text-muted fa-clock-o"></i></span>
                        </div>
                        <div id="{{ $activity->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body bg-muted">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title">
                                                <h5>Datos Viejo</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="feed-activity-list">
                                                    @forelse ($activity->old_values as $key => $custom)
                                                        <div class="feed-element">
                                                            {{ $key }}: {{ $custom }}  
                                                        </div>                           
                                                    @empty
                                                        <div class="feed-element">
                                                            Sin detalles  
                                                        </div>    
                                                    @endforelse
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title">
                                                <h5>Datos Nuevos</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="feed-activity-list">
                                                    @forelse ($activity->new_values as $key => $custom)
                                                        <div class="feed-element">
                                                            {{ $key }}: {{ $custom }}  
                                                        </div>                           
                                                    @empty
                                                        <div class="feed-element">
                                                            Sin detalles  
                                                        </div>    
                                                    @endforelse
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    Sin actividades
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection