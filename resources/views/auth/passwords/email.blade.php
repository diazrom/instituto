@extends('layouts.public')

@section('title', 'Login')

@section('content')
    <div>
        <h1 class="logo-name">HT+</h1>
    </div>
    <h3>Welcome to HT+</h3>
    <p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
        <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
    </p>
    <p>Login in. To see it in action.</p>
    <form class="m-t" role="form" action="{{ route('password.email') }}" method="POST">
            {{ csrf_field() }}
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required autofocus>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        
        <button type="submit" class="btn btn-primary block full-width m-b">Send Password Reset Link</button>
    </form>
    <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
@endsection