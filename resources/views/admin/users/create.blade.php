@extends('layouts.app')

@section('title', 'Crear usuario')

@section('breadcrumb')
<li>
    <a href="{{ route('users.index') }}">usuarios</a>
</li>
<li class="active">
    <strong>crear<strong>
</li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-offset-3 col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Crear usuario <small>todos los capos son requeridos</small></h5>
            </div>
            <div class="ibox-content">
                <form action="{{ route('users.store') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Contraseña</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Confirmar contraseña</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" name="password_confirmation">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}"><label class="col-sm-3 control-label">Activo</label>
                        <div class="col-sm-8">
                            <div class="onoffswitch">
                                <input type='hidden' value='0' name='active'>
                                <input 
                                    name="active" 
                                    value="{{ old('active') ?? 1 }}" 
                                    type="checkbox" 
                                    class="onoffswitch-checkbox" 
                                    id="active" 
                                >
                                <label class="onoffswitch-label" for="active">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                                @if ($errors->has('active'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('active') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Roles</label>
                        <div class="col-sm-8">
                            <form-select uri="{{ route('roles.index') }}" name="roles"></form-select>
                            @if ($errors->has('roles'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('roles') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary pull-right">Crear Usuario</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection