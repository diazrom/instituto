@extends('layouts.app')

@section('title', 'Usuarios')

@section('breadcrumb')
<li class="active">
    <strong>usuarios</strong>
</li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Lista de usuarios </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <span class="pull-right"><a class="btn btn-primary" href="{{ route('users.create') }}">Crear Usuario</a></span>                    
                    <table class="table table-striped table-responsive" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Activo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse ($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <div class="switch">
                                        <form action="{{ route('users.update', $user) }}" method="POST">
                                            {{ method_field('PUT') }} 
                                            {{ csrf_field() }}
                                            <div class="onoffswitch">
                                                <input type='hidden' value='0' name='active'>
                                                <input 
                                                    name="active" 
                                                    value="1" 
                                                    type="checkbox" 
                                                    class="onoffswitch-checkbox" 
                                                    id="active-{{ $user->id }}" 
                                                    onChange="this.form.submit()" 
                                                    {{ $user->active ? 'checked': null }} 
                                                    {{ $user->isAdmin() ? 'disabled': null }}
                                                >
                                                <label class="onoffswitch-label" for="active-{{ $user->id }}">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </form>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ route('users.edit', $user) }}" class="text-warning" title="Editar">
                                        <i class="fa fa-pencil-square-o fa-2x"></i>
                                    </a>
                                    @can('delete', $user)
                                        <form id="delete-user-{{ $user->id }}" action="{{ route('users.destroy', $user) }}" method="POST" style="display: none;" onSubmit="return window.confirm(Estas seguro?);">
                                                {{ method_field('DELETE') }} 
                                                {{ csrf_field() }}
                                        </form>
                                        <a href="javascript:void(0);" class=" text-danger" onClick="event.preventDefault(); document.getElementById('delete-user-{{ $user->id }}').submit();"  title="Eliminar">
                                            <i class="fa fa-trash-o fa-2x"></i>
                                        </a>
                                    @endcan
                                    <a href="{{ route('change-password.edit', $user) }}" class="text-success" title="Cambiar contraseña">
                                        <i class="fa fa-key fa-2x"></i>
                                    </a>
                                    <a href="{{ route('logs.show', ['user', $user->id]) }}" class="text-muted" title="Log del usuario">
                                        <i class="fa fa-file-text-o fa-2x"></i>     
                                    </a>
                                    <a href="{{ route('logs.show', ['user', $user->id, true]) }}"  class="text-primary" title="Log sobre el usuario">
                                        <i class="fa fa-file-text fa-2x"></i>     
                                    </a> 
                                    @can('delete', $user)
                                        @canImpersonate('impersonate', $user)
                                            <a href="{{ route('impersonate', $user->id) }}"  style="color: black;" title="Personificar">
                                                <i class="fa fa-user-secret fa-2x"></i>
                                            </a>
                                        @endCanImpersonate
                                    @endcan
                                </td>
                            </tr>
                        @empty
                            No hay Usuario
                        @endforelse
                        </tbody>
                    </table>
                    <span class="pull-right">{{ $users }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection