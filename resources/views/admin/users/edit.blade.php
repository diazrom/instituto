@extends('layouts.app')

@section('title', 'Editar usuario')

@section('breadcrumb')
<li>
    <a href="{{ route('users.index') }}">usuarios</a>
</li>
<li class="active">
    <strong>editar</strong>
</li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-offset-3 col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Creando usuario <small>todos los capos son requeridos</small></h5>
            </div>
            <div class="ibox-content">
                <form action="{{ route('users.update', $user) }}" method="POST" class="form-horizontal" role="form">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="name" value="{{ old('name') ?? $user->name }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" name="email" value="{{ old('email') ?? $user->email }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('permissions') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Permisos</label>
                        <div class="col-sm-8">
                            @php 
                                $data = $user->roles->map(function($role) {
                                    return ['label' => $role->name, 'key' => $role->id ];
                                })->toJson();
                            @endphp
                            <form-select :data-value="{{ $data }}" uri="{{ route('roles.index') }}" name="roles"></permission-select>
                            @if ($errors->has('permissions'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('permissions') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary pull-right">Guardar cambios</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection