@extends('layouts.app')

@section('title', 'Permisos')

@section('breadcrumb')
<li class="active">
    <strong>permisos</strong>
</li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Lista de permisos </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                <form action="{{ route('permissions.store') }}" method="POST">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary pull-right">Actualizar Permisos</button>
                    </form>
                    <table class="table table-striped table-responsive" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Identificador</th>
                                <th>Activo</th>
                                <th>Acciones</th>
                        </thead>
                        <tbody>
                        @forelse ($permissions as $permission)
                            <tr>
                                <td>{{ $permission->name }}</td>
                                <td>{{ $permission->ident }}</td>
                                <td>
                                    <div class="switch">
                                        <form action="{{ route('permissions.update', $permission) }}" method="POST">
                                            {{ method_field('PUT') }} 
                                            {{ csrf_field() }}
                                            <div class="onoffswitch">
                                                <input type='hidden' value='0' name='active'>
                                                <input 
                                                    name="active" 
                                                    value="1" 
                                                    type="checkbox" 
                                                    class="onoffswitch-checkbox" 
                                                    id="active-{{ $permission->id }}" 
                                                    onChange="this.form.submit()" 
                                                    {{ $permission->active ? 'checked': null }} 
                                                >
                                                <label class="onoffswitch-label" for="active-{{ $permission->id }}">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </form>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ route('permissions.edit', $permission) }}" class="text-warning" title="Editar">
                                        <i class="fa fa-pencil-square-o fa-2x"></i>
                                    </a>
                                    <a href="{{ route('logs.show', ['permission', $permission->id, true]) }}"  class="text-primary" title="Log sobre el usuario">
                                        <i class="fa fa-file-text fa-2x"></i>     
                                    </a>
                                </td>
                            </tr>
                        @empty
                            No hay permisos
                        @endforelse
                        </tbody>
                    </table>
                    <span class="pull-right">{{ $permissions }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection