@extends('layouts.app')

@section('title', 'Editar Rol')

@section('breadcrumb')
<li>
    <a href="{{ route('roles.index') }}">roles</a>
</li>
<li class="active">
    <strong>editar<strong>
</li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-offset-3 col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Editar rol <small>todos los capos son requeridos</small></h5>
            </div>
            <div class="ibox-content">
                <form action="{{ route('roles.update', $role) }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="name" value="{{ old('name') ?? $role->name }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Descripcion</label>
                        <div class="col-sm-8">
                            <input type="description" class="form-control" name="description" value="{{ old('description') ?? $role->description }}">
                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('permissions') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Permisos</label>
                        <div class="col-sm-8">
                            @php 
                                $data = $role->permissions->map(function($permission) {
                                    return ['label' => $permission->name, 'key' => $permission->id ];
                                })->toJson();
                            @endphp
                            <form-select :data-value="{{ $data }}" uri="{{ route('permissions.index') }}" name="permissions"></permission-select>
                            @if ($errors->has('permissions'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('permissions') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary pull-right">Guardar Cambios</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection