@extends('layouts.app')

@section('title', 'Roles')

@section('breadcrumb')
<li class="active">
    <strong>roles</strong>
</li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Lista de roles </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <span class="pull-right"><a class="btn btn-primary" href="{{ route('roles.create') }}">Crear Rol</a></span>                    
                    <table class="table table-striped table-responsive" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Acciones</th>
                        </thead>
                        <tbody>
                        @forelse ($roles as $role)
                            <tr>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->description }}</td>
                                <td>
                                    <a href="{{ route('roles.edit', $role) }}" class="text-warning" title="Editar">
                                        <i class="fa fa-pencil-square-o fa-2x"></i>
                                    </a>
                                    <a href="{{ route('logs.show', ['role', $role->id, true]) }}"  class="text-primary" title="Log sobre el usuario">
                                        <i class="fa fa-file-text fa-2x"></i>     
                                    </a>
                                </td>
                            </tr>
                        @empty
                            No hay roles
                        @endforelse
                        </tbody>
                    </table>
                    <span class="pull-right">{{ $roles }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection