@extends('layouts.app')

@section('title', 'Crear rol')

@section('breadcrumb')
<li>
    <a href="{{ route('roles.index') }}">roles</a>
</li>
<li class="active">
    <strong>crear<strong>
</li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-offset-3 col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Crear rol <small>todos los capos son requeridos</small></h5>
            </div>
            <div class="ibox-content">
                <form action="{{ route('roles.store') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Descripcion</label>
                        <div class="col-sm-8">
                            <input type="description" class="form-control" name="description" value="{{ old('description') }}">
                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group{{ $errors->has('permissions') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label">Permisos</label>
                        <div class="col-sm-8">
                            <form-select uri="{{ route('permissions.index') }}" name="permissions"></form-select>
                            @if ($errors->has('permissions'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('permissions') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary pull-right">Crear Rol</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection