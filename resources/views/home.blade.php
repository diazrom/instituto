@extends('layouts.app') 

@section('title', 'Dashboard') 

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="col-lg-3">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<span class="label label-warning pull-right">Mensual</span>
					<h5>Trabajadores</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins">140</h1>
					<div class="stat-percent font-bold text-success">80% <i class="fa fa-bolt"></i></div>
					<small>Total</small>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<span class="label label-info pull-right">Anual</span>
					<h5>Contratos</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins">275</h1>
					<div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
					<small>Total Contratos</small>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<span class="label label-success pull-right">Mensual</span>
					<h5>Nuevos Contratos</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins">4</h1>
					<div class="stat-percent font-bold text-success">8% <i class="fa fa-bolt"></i></div>
					<small>Nuevos</small>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<span class="label label-danger pull-right">Semanal</span>
					<h5>Visitas</h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins">40</h1>
					<div class="stat-percent font-bold text-info">2% <i class="fa fa-level-up"></i></div>
					<small>Total Visitas</small>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Actividad</h5>
				<span class="label label-primary">Hoy</span>
				<div class="ibox-tools">
					<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					<a class="close-link">
							<i class="fa fa-times"></i>
						</a>
				</div>
			</div>

			<div class="ibox-content inspinia-timeline">

				<div class="timeline-item">
					<div class="row">
						<div class="col-xs-3 date">
							<i class="fa fa-briefcase"></i> 6:00 am
							<br>
							<small class="text-navy">2 hour ago</small>
						</div>
						<div class="col-xs-7 content no-top-border">
							<p class="m-b-xs"><strong>Meeting</strong></p>

							<p>Conference on the sales results for the previous year. Monica please examine sales trends in marketing and products.</p>

						</div>
					</div>
				</div>
				<div class="timeline-item">
					<div class="row">
						<div class="col-xs-3 date">
							<i class="fa fa-file-text"></i> 7:00 am
							<br>
							<small class="text-navy">3 hour ago</small>
						</div>
						<div class="col-xs-7 content">
							<p class="m-b-xs"><strong>Send documents to Mike</strong></p>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
								dummy text ever since.</p>
						</div>
					</div>
				</div>
				<div class="timeline-item">
					<div class="row">
						<div class="col-xs-3 date">
							<i class="fa fa-coffee"></i> 8:00 am
							<br>
						</div>
						<div class="col-xs-7 content">
							<p class="m-b-xs"><strong>Coffee Break</strong></p>
							<p>
								Go to shop and find some products. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
								has been the industry's.
							</p>
						</div>
					</div>
				</div>
				<div class="timeline-item">
					<div class="row">
						<div class="col-xs-3 date">
							<i class="fa fa-phone"></i> 11:00 am
							<br>
							<small class="text-navy">21 hour ago</small>
						</div>
						<div class="col-xs-7 content">
							<p class="m-b-xs"><strong>Phone with Jeronimo</strong></p>
							<p>
								Lorem Ipsum has been the industry's standard dummy text ever since.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Archivos Conciliación</h5>
				<div class="ibox-tools">
					<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					<a class="close-link">
							<i class="fa fa-times"></i>
						</a>
				</div>
			</div>
			<div class="ibox-content no-padding">
				<ul class="list-group">
				@if(isset($arreglo))
					@foreach($arreglo as $ar)
						@if(strtotime($ar['date']) >= $fec)
							<li class="list-group-item">
								<p><a class="text-info" href="{{ route('home.descargar', $ar['name']) }}">{{ $ar['name'] }}</a></p>
								<small class="block text-muted"><i class="fa fa-clock-o"></i> {{ $ar['date'] }}</small>
							</li>
						@endif
					@endforeach
				@endif
				</ul>
			</div>
		</div>
	</div>
</div>
@endsection