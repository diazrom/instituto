<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe tener al menos 6 caracteres y coincidir con la confirmación.',
    'reset' => '¡Su contraseña ha sido restablecida!',
    'sent' => '¡Recordatorio de contraseña enviado!',
    'token' => 'Este token de restablecimiento de contraseña es inválido.',
    'user' => 'No se ha encontrado un usuario con esa dirección de correo.',
    'current_password' => [
        'match' => 'La contraseña ingresada no coincide con la contraseña que proporcionó. Por favor vuelva a intenterlo.',
        'same' => 'Tu nueva contraseña no puede ser la misma que tu actual contraseña. Por favor ingrasa una nueva contraseña.'
        ]
];
