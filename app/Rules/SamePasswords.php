<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SamePasswords implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value !== request()->get('new_password');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('passwords.current_password.same');
    }
}
