<?php

namespace App;

use App\User;
use App\Permission;
use App\Traits\CacheTrait;
use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordsActivity;

class Role extends Model
{
    use CacheTrait, RecordsActivity;

    protected $fillable = ['name', 'description'];

    // Eager loading
    protected $with = ['permissions'];

    // Relationships
    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class)->withTimestamps();
        ;
    }

    // Relationship methods
    public function assignPermission($permissionId = null)
    {
        $permissions = $this->getCacheRelationship('permissions', true)->pluck('id');
        if (!$permissions->contains($permissionId)) {
            $this->flushCache();

            return $this->permissions()->attach($permissionId);
        }
        return false;
    }

    public function revokePermission($permissionId = null)
    {
        if (!empty($permissionId)) {
            $this->flushCache();

            return $this->permissions()->detach($permissionId);
        }
    }

    public function syncPermissions($permissionIds = null)
    {
        if (!empty($permissionId)) {
            $this->flushCache();

            return $this->permissions()->sync($permissionIds, false);
        }
    }

    public function revokeAllPermissions()
    {
        $this->flushCache();

        return $this->permissions()->detach();
    }
}
