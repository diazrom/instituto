<?php

if (!function_exists('isActiveRoute')) {
    function isActiveRoute($route, $output = 'active')
    {
        if (strpos(Route::currentRouteName(), $route) !== false) {
            return $output;
        }
    }
}
