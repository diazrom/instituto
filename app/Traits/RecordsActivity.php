<?php

namespace App\Traits;

use App\Activity;
use ReflectionClass;

trait RecordsActivity
{
    /**
     * Register the necessary event listeners.
     *
     * @return void
     */
    protected static function bootRecordsActivity()
    {
        if (auth()->guest()) {
            return;
        }

        foreach (static::getModelEvents() as $event) {
            static::$event(function ($model) use ($event) {
                if (!$model->dontRecordsHiddenAttributes($model, $event)) {
                    return;
                }
                $model->recordActivity($event, $model);
            });
        }
    }

    /**
     * Record activity for the model.
     *
     * @param  string $event
     * @return void
     */
    public function recordActivity($event, $model)
    {
        Activity::create([
            'activitable_id' => $model->id,
            'activitable_type' => get_class($model),
            'event' => $model->getActivityName($model, $event),
            'user_id' => auth()->user()->id,
            'old_values' => $model->getOldValues($model, $event),
            'new_values' => $model->getDirty(),
            'current_values' => $model->fresh()->toArray()
        ]);
    }

    /**
     * Prepare the appropriate activity name.
     *
     * @param  mixed  $model
     * @param  string $action
     * @return string
     */
    protected function getActivityName($model, $action)
    {
        $name = strtolower((new ReflectionClass($model))->getShortName());
        $action = str_replace('ing', 'ed', $action);
        return "{$action}_{$name}";
    }

    /**
     * Get the model events to record activity for.
     *
     * @return array
     */
    protected static function getModelEvents()
    {
        return [
            'created', 'deleting', 'updating'
        ];
    }

    private function dontRecordsHiddenAttributes($model, $event)
    {
        if (count(array_intersect($model->hidden, array_keys($model->getDirty()))) == 0 || $event == 'created') {
            return true;
        }

        return false;
    }

    private function getOldValues($model, $event)
    {
        $modelToArray = $model->fresh() ? $model->fresh()->toArray() : [];
        if ($event == 'created') {
            $modelToArray = [];
        }
        $old_values = array_intersect_key($modelToArray, $model->getDirty());
        if ($event == 'deleting') {
            $old_values = $modelToArray;
        }

        return $old_values;
    }
}
