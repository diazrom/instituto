<?php

namespace App\Http\Controllers\TipoDocumento;

use App\TipoDocumento;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TipoDocumentoController extends Controller
{
    //
 
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $tipos = TipoDocumento::orderBy('id', 'ASC')->paginate(20);
        if (request()->wantsJson()) {
            return $tipos;
        }
        return view('tipodocumento.index', compact('tipos'));
    }

    public function create()
    {
        return view('tipodocumento.create');
    }

    protected function store(Request $data)
    {
        $validatedData = $data->validate([
            'name' => 'required|string|max:255',
            'softland' => 'required|string|max:2',
            'banco_id' => 'required|numeric',
            'estado' => 'required'
        ]);
        $docu = new TipoDocumento;
        $docu->fill($validatedData);
        $docu->save();
        return redirect(route('tipodocumento.index'))->with(['flash' => 'haz creado correctamente un evento', 'type' => 'success']);
    }

    public function edit($doc)
    {
        $tipo = TipoDocumento::where('id',$doc)->first();
        return view('tipodocumento.edit', compact('tipo'));
    }

    public function update(Request $request,$doc)
    {
        //$this->authorize('update', $evento);
        $tipo = TipoDocumento::where('id',$doc)->first();
        $validatedData = $request->validate([
            'name' => 'sometimes|required|string|max:255',
            'softland' => 'sometimes|required|string|max:2',
            'banco_id' => 'sometimes|required|numeric',
            'estado' => 'sometimes|required'
        ]);
        $tipo->fill($validatedData);
        $tipo->save();
        if (request()->wantsJson()) {
            return response()->json(['updated' => true, 'code' => 202], 202);
        }

        return redirect(route('tipodocumento.index'))->with(['flash' => 'haz actualizado correctamente un evento', 'type' => 'success']);
    }

}
