<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Rules\PasswordsMatches;
use App\Rules\SamePasswords;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\User  $$user
    * @return \Illuminate\Http\Response
    */
    public function edit(User $user)
    {
        return view('change-password', compact('user'));
    }

    /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \App\User  $$user
         * @return \Illuminate\Http\Response
         */
    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);

        $validatedData = $request->validate([
            'current_password' => ['required', new PasswordsMatches, new SamePasswords],
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user->password = bcrypt($validatedData['password']);
        $user->save();

        return redirect()->back();
    }
}
