<?php

namespace App\Http\Controllers;
use App\TipoDocumento;

use Illuminate\Http\Request;

class FinanzasController extends Controller
{
    //
 
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	return view('finanza.index');
    }

    protected function cargar(Request $archivo){
        $validatedData = $archivo->validate([
            'file' => 'required|mimes:txt|max:2048'
        ]);
        $tipo_pagos = TipoDocumento::where('estado',1)->pluck('softland','name');
        $name = $archivo->file('file')->getClientOriginalName();
        $archivo->file('file')->move(public_path().'/files/', $name);
        $ruta = public_path().'/files/'.$name;
        $file_get = fopen($ruta, 'r');
        $num = 0;
        while(!feof($file_get)){
        	$linea = fgets($file_get);
            $pal[] = explode(';', $linea);
            $valor = explode(';', $linea);
            if(isset($valor[7])){
                $sep = explode(' ', trim($valor[7]));
                $tipo_p[] = $sep[0];
            }
            if(isset($valor[9]))
                $sum = $valor[9];
            $num++;
        }
        unset($pal[$num-1]);
        foreach ($pal as $key => $p) {
            $cuenta = substr($p[12], 6);
            $ini_date = '01-'.substr($p[1], 2,2).'-'.substr($p[11], 4);
            $fin_date = date('t-m-Y',strtotime($ini_date));
//            $mes = (int)(substr($p[11], 2,2));
            $mes = substr($p[1], 2,2);
            $anio = substr($p[11], 4);
/*            if($mes < 10)
                $string = $mes.'00'.$cuenta."\t";
            else*/
                $string = $mes.'0'.$cuenta."\t";                
            $string .= substr($p[11], 4)."\t";
            $string .= $p[0]."\t";
            $string .= str_replace('-', '/', $ini_date)."\t";
            $string .= str_replace('-', '/', $fin_date)."\t";
            $string .= (int)$sum."\r\n";
            break;
        }
        $fila = 1;
        foreach ($pal as $key => $p) {
//            $cuenta = substr($p[12], 6);
//            $ini_date = substr($p[11], 0,2).'-'.substr($p[11], 2,2).'-'.substr($p[11], 4);
            $ini_date = substr($p[1], 0,2).'-'.substr($p[1], 2,2).'-'.substr($p[11], 4);
            $fin_date = date('t-m-Y',strtotime($ini_date));
            $string .= $fila."\t".str_replace('-', '/', $ini_date)."\t";
            if(isset($tipo_pagos[$tipo_p[$key]]))
                $string .= $tipo_pagos[$tipo_p[$key]]."\t".$p[2]."\t";
            else{
                $err[] = $tipo_p[$key];
                $string .= $tipo_p[$key]."\t".$p[2]."\t";                
            }
            if($p[5] == 'C')
                $string .= $p[8]."\t\r\n";
            else
                $string .= "\t".$p[8]."\r\n";
            $fila++;                
        }
        if(empty($err)){
            $ruta_2 = public_path().'/files/conciliacion/'.date('dmy').'_conciliacion_'.$cuenta.'.txt';
            $fw = fopen($ruta_2, 'w');
            $wfile = fputs($fw, $string);
            fclose($fw);
            $encabezado = "Content-Disposition: attachment; filename=\"".date('dmy')."_conciliacion_".$mes.''.$anio.'_'.$cuenta.".txt\"";
            $tipo = "Content-type: text/plain";
            header($encabezado);
            header($tipo);
            readfile(public_path().'/files/conciliacion/'.date('dmy').'_conciliacion_'.$cuenta.'.txt');
            exit();
        }else
            return view('finanza.index', compact('err'));
    }
}
