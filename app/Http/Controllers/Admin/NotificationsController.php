<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
    public function index()
    {
        $notifications = auth()->user()->notifications;
        if (request()->wantsJson()) {
            return $notifications;
        }
        return view('notifications.index', compact('notifications'));
    }

    public function update()
    {
        auth()->user()->notifications->markAsRead();
        return redirect()->route('messages.index');
    }
}
