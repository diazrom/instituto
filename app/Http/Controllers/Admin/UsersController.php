<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::orderBy('id', 'ASC')->paginate(10);
        if (request()->wantsJson()) {
            return $users;
        }
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'active' => 'nullable|min:0|max:1',
            'roles' => 'required'
        ]);

        $user = new User;
        $validatedData['password'] = bcrypt($validatedData['password']);
        $user->fill($validatedData);
        $user->save();
        $user->revokeAllRoles();
        $user->assignRole($validatedData['roles']);

        if (request()->wantsJson()) {
            return response()->json(['created' => true, 'code' => 201], 201);
        }

        return redirect(route('users.index'))->with(['flash' => 'haz creado correctamente un usuario', 'type' => 'success']);
    }

    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);
        $validatedData = $request->validate([
            'name' => 'sometimes|required|string|max:255',
            'email' => 'sometimes|required|string|email|max:255|unique:users,email,' . $user->id,
            'active' => 'sometimes|required|boolean',
            'roles' => 'sometimes'
        ]);

        $user->fill($validatedData);
        $user->save();
        $user->revokeAllRoles();
        $user->assignRole($validatedData['roles']);
        if (request()->wantsJson()) {
            return response()->json(['updated' => true, 'code' => 202], 202);
        }

        return redirect(route('users.index'))->with(['flash' => 'haz actualizado correctamente un usuario', 'type' => 'success']);
    }

    public function destroy(User $user)
    {
        $this->authorize('delete', $user);

        $user->delete();
        if (request()->wantsJson()) {
            return response()->json(['deleted' => true, 'code' => 202], 202);
        }
        return redirect(route('users.index'))->with(['flash' => 'haz eliminado correctamente un usuario', 'type' => 'error']);
    }
}
