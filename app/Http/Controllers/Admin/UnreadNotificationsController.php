<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UnreadNotificationsController extends Controller
{
    public function index()
    {
        $notifications = auth()->user()->unreadNotifications;
        if (request()->wantsJson()) {
            return $notifications;
        }
        return view('notifications.index', compact('notifications'));
    }

    public function update()
    {
        $notification = auth()->user()->notifications->where('id', request('id'))->first();
        if ($notification->read_at === null) {
            $notification->markAsRead();
        }
    }
}
