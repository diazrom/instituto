<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Permission;

class RolesController extends Controller
{
    public function index()
    {
        $roles = Role::orderBy('id', 'ASC')->paginate(10);
        if (request()->wantsJson()) {
            return Role::all();
        }
        return view('admin.roles.index', compact('roles'));
    }

    public function create()
    {
        return view('admin.roles.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255|unique:roles',
            'description' => 'required|string|max:255',
            'permissions' => 'required'
        ]);

        $role = new Role;
        $role->fill($validatedData);
        $role->save();
        $role->revokeAllPermissions();
        $role->assignPermission($validatedData['permissions']);

        if (request()->wantsJson()) {
            return response()->json(['created' => true, 'code' => 201], 201);
        }

        return redirect(route('roles.index'))->with(['flash' => 'haz creado correctamente un rol', 'type' => 'success']);
    }

    //NOTICE: No cuenta con visualizacion de rol
    // public function show(Role $role)
    // {
    //     return view('admin.roles.show', compact('role'));
    // }

    public function edit(Role $role)
    {
        return view('admin.roles.edit', compact('role'));
    }

    public function update(Request $request, Role $role)
    {
        $validatedData = $request->validate([
            'name' => 'sometimes|required|string|max:255|unique:roles,name,' . $role->id,
            'description' => 'sometimes|required|string|max:255',
            'permissions' => 'sometimes|required'
        ]);

        $role->fill($validatedData);
        $role->save();
        $role->revokeAllPermissions();
        $role->assignPermission($validatedData['permissions']);
        if (request()->wantsJson()) {
            return response()->json(['updated' => true, 'code' => 202], 202);
        }

        return redirect(route('roles.index'))->with(['flash' => 'haz actualizado correctamente un rol', 'type' => 'success']);
    }
}
