<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class PermissionsController extends Controller
{
    public function index()
    {
        $permissions = Permission::orderBy('id', 'ASC')->paginate(20);
        if (request()->wantsJson()) {
            return Permission::active()->get();
        }
        return view('admin.permissions.index', compact('permissions'));
    }

    public function store()
    {
        $permissions = Permission::pluck('ident');
        $routeCollection = collect(Route::getRoutes());
        $filtered = $routeCollection->filter(function ($route) use ($permissions) {
            return !$permissions->contains($route->getActionName()) && collect($route->gatherMiddleware())->contains('permissions.overseer');
        })->map(function ($filter) {
            return [
                'ident' => $filter->getActionName(),
                'name' => $filter->getName(),
                'created_at' => 'now',
                'updated_at' => 'now'
            ];
        });

        if ($filtered->isEmpty()) {
            return redirect(route('permissions.index'))->with(['flash' => 'No existen permisos nuevos que insertar', 'type' => 'warn']);
        }

        Permission::insert($filtered->toArray());
        return redirect(route('permissions.index'))->with(['flash' => 'haz insertado correctamente los permisos nuevo', 'type' => 'success']);
    }

    public function edit(Permission $permission)
    {
        return view('admin.permissions.edit', compact('permission'));
    }

    public function update(Request $request, Permission $permission)
    {
        $validatedData = $request->validate([
            'name' => 'sometimes|required|string|max:255',
            'active' => 'sometimes|required|boolean'
        ]);

        $permission->fill($validatedData);
        $permission->save();
        if (request()->wantsJson()) {
            return response()->json(['updated' => true, 'code' => 202], 202);
        }

        return redirect(route('permissions.index'))->with(['flash' => 'haz actualizado correctamente un permiso', 'type' => 'success']);
    }
}
