<?php

namespace App\Http\Controllers\Admin;

use App\Activity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activities = Activity::orderBy('created_at', 'desc')->get();
        return view('activities.index', compact('activities'));
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $model
     * @return \Illuminate\Http\Response
     */
    public function show($model, $id, $yourself = false)
    {
        $modelType = 'App\\' . ucfirst($model);
        $model = $modelType::find($id);
        if ($yourself) {
            $activities = Activity::where('activitable_id', $model->id)->where('activitable_type', $modelType)->orderBy('created_at', 'desc')->get();
        } else {
            $activities = $model->activity;
        }
        return view('activities.show', compact('activities'));
    }
}
