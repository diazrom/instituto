<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carpeta = opendir(public_path().'/files/conciliacion/');
        $i = 0;
        $fec = strtotime(date("d-m-Y", strtotime(date('d-m-Y')." - 2 month")));
        while ($arch = readdir($carpeta)) {
            if(!is_dir($arch)){
                $ar_name[$i]['name'] = $arch;
                $ar_name[$i]['date'] = date('d-m-Y H:i:s',filemtime(public_path().'/files/conciliacion/'.$arch));
            $i++;
            }
        }
        if(!empty($ar_name)){
            $arreglo = collect($ar_name);
            return view('home', compact('arreglo','fec'));
        }else
            return view('home');
    }

    public function descargar($name){
        $ruta = public_path().'/files/conciliacion/'.$name;
        $file_get = fopen($ruta, 'r');
        $encabezado = "Content-Disposition: attachment; filename=\"".$name."\"";
        $tipo = "Content-type: text/plain";
        header($encabezado);
        header($tipo);
        readfile(public_path().'/files/conciliacion/'.$name);
        exit();        
    }

    public function show()
    {
        $user = factory('App\User')->create();
        $user->name = 'fabian';
        $user->save();

        return 'Hola';
    }
}
