<?php

namespace App;

use App\Traits\CacheTrait;
use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordsActivity;

class Permission extends Model
{
    use CacheTrait, RecordsActivity;
    // Mass assignment
    protected $fillable = ['ident', 'name', 'active'];

    // Relationships
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    // Scopes

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    // Relationship methods
    public function assignRole($roleId = null)
    {
        $roles = $this->getCacheRelationship('roles', true)->pluck('id');
        if (!$roles->contains($roleId)) {
            $this->flushCache();
            return $this->roles()->attach($roleId);
        }
    }

    public function revokeRole($roleId = null)
    {
        if (!empty($roleId)) {
            $this->flushCache();
            return $this->roles()->detach((array)$roleId);
        }
    }

    public function syncRoles($roleId = null)
    {
        if (!empty($roleId)) {
            $this->flushCache();
            return $this->roles()->sync((array)$roleId);
        }
    }

    public function revokeAllRoles()
    {
        $this->flushCache();
        return $this->roles()->detach();
    }
}
