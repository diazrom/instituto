<?php

namespace App;

use App\Traits\OverseerTrait;
use App\Traits\RecordsActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Lab404\Impersonate\Models\Impersonate;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, OverseerTrait, RecordsActivity, Impersonate, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the activity timeline for the user.
     *
     * @return mixed
     */
    public function activity()
    {
        return $this->hasMany('App\Activity')
            ->with(['user', 'activitable'])
            ->latest();
    }

    public function canImpersonate()
    {
        return $this->isAdmin();
    }
}
