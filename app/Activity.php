<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $guarded = [];

    protected $casts = [
        'old_values' => 'json',
        'new_values' => 'json',
        'current_values' => 'json'
    ];

    protected $with = ['user', 'activitable'];

    /**
     * Get the user responsible for the given activity.
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the subject of the activity.
     *
     * @return mixed
     */
    public function activitable()
    {
        return $this->morphTo();
    }

    public function getElapsedTimeAttribute()
    {
        return !$this->created_at ?: $this->created_at->diffForHumans();
    }
}
