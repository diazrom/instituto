<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();


Route::resource('change-password', 'ChangePasswordController', ['only' => ['edit', 'update'], 'parameters' => ['change-password' => 'user']]);

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['permissions.overseer']], function () {
        Route::get('/', 'HomeController@index')->name('home');
        Route::get('/home/descargar/{name}', 'HomeController@descargar')->name('home.descargar');
        Route::get('/finanza/index','FinanzasController@index')->name('finanza.index');
        Route::post('/finanza/cargar','FinanzasController@cargar')->name('finanza.cargar');

        Route::resource('/tipodocumento','TipoDocumento\TipoDocumentoController');
        Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
            Route::impersonate();
            Route::resource('users', 'UsersController', ['except' => ['show']]);
            Route::resource('permissions', 'PermissionsController', ['except' => ['show', 'create', 'destroy']]);
            Route::resource('roles', 'RolesController');
            Route::get('/logs', 'ActivitiesController@index')->name('logs.index');
            Route::get('/logs/{model}/{id}/{yourself?}', 'ActivitiesController@show')->name('logs.show');
            Route::resource('notifications', 'NotificationsController', ['only' => ['index', 'update']]);
            Route::resource('unread-notifications', 'UnreadNotificationsController', ['only' => ['index', 'update']]);
        });
    });
});
