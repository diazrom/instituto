<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = factory(Role::class)->create([
            'name' => 'Admin',
            'description' => 'Administrator'
        ]);

        $role->assignPermission(1);
    }
}
