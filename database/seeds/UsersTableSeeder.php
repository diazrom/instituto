<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(User::class)->create([
            'name' => 'Fabian Olivares Rios',
            'email' => 'f.olivaresrios@gmail.com',
            'active' => true
        ]);

        $user->assignRole(1);
    }
}
