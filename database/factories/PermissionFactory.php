<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Permission::class, function (Faker $faker) {
    return [
        'ident' => '*',
        'name' => 'admin-bypass',
        'active' => true
    ];
});
