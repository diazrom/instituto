<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function adminUser()
    {
        $user = factory('App\User')->create(['active' => true]);
        $role = factory('App\Role')->create();
        $permission = factory('App\Permission')->create();

        $role->assignPermission($permission->id);
        $user->assignRole($role->id);

        return $user->fresh();
    }

    public function normalUser()
    {
        $user = factory('App\User')->create(['active' => true]);
        $role = factory('App\Role')->create(['name' => 'normal-user' . rand(0, 1000), 'description' => 'Normal User']);
        $permission[] = factory('App\Permission')->create(['ident' => str_random(20), 'name' => 'home.index']);
        $permission[] = factory('App\Permission')->create(['ident' => str_random(20), 'name' => 'home.show']);
        $permission[] = factory('App\Permission')->create(['ident' => str_random(20), 'name' => 'home.store']);

        $role->assignPermission($permission[rand(0, 2)]->id);
        $user->assignRole($role->id);

        return $user->fresh();
    }
}
