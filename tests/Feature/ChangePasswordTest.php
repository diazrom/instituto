<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Http\Middleware\UserHasPermission;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ChangePasswordTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_user_change_your_password()
    {
        $this->withoutMiddleware(UserHasPermission::class);

        $user = $this->normalUser();
        $passwordTest = '123456';
        $user->password = bcrypt($passwordTest);
        $user->save();

        $user = $user->fresh();

        $request = $this->actingAs($user)->from(route('users.index'))
                        ->put(
                            route('change-password.update', $user),
                            [
                                'current_password' => $passwordTest,
                                'password' => 'secret',
                                'password_confirmation' => 'secret'
                            ]
                        );
        $request->assertRedirect(route('users.index'));
    }

    /** @test */
    public function can_user_view_change_password_view()
    {
        $this->withoutMiddleware(UserHasPermission::class);

        $user = $this->normalUser();

        $request = $this->actingAs($user)->from(route('users.index'))->put(route('change-password.edit', $user));
        $request->assertSee('Contraseña actual');
        $request->assertSee('Nueva contraseña');
        $request->assertSee('Confirmar nueva contraseña');
    }
}
