<?php

namespace Tests\Feature\Admin\Permissions;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminUpdatePermissionTest extends TestCase
{
    use RefreshDatabase;

    private function validParams($overrides = [])
    {
        return array_merge([
            'name' => 'fake name',
            'ident' => 'fake description',
        ], $overrides);
    }

    /** @test */
    public function can_admin_update_a_permission()
    {
        $userAdmin = $this->adminUser();
        $permission = factory('App\Permission')->create(['name' => 'fake name permission', 'ident' => 'hola']);
        $request = $this->actingAs($userAdmin)->from(route('permissions.index'))->patch(route('permissions.update', $permission), ['name' => 'unique']);
        $this->assertDatabaseHas('permissions', ['name' => 'unique']);
        $request->assertRedirect(route('permissions.index'));
    }

    /** @test */
    public function name_is_required_sometimes()
    {
        $user = $this->adminUser();
        $permission = factory('App\Permission')->create(['name' => 'fake name permission', 'ident' => 'hola']);
        $request = $this->actingAs($user)->patch(route('permissions.update', $permission), [
            'name' => 'secret',
        ]);

        $this->assertDatabaseHas('permissions', ['name' => 'secret']);
    }
}
