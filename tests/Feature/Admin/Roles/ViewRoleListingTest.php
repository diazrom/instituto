<?php

namespace Tests\Feature\Admin\Roles;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewRoleListingTest extends TestCase
{
    use RefreshDatabase;

    /**
    * @test
    */
    public function can_admin_view_roles_listing_as_json_format()
    {
        $user = $this->adminUser();
        $role = factory('App\Role')->create(['name' => 'fake name role']);
        $role2 = factory('App\Role')->create(['name' => 'fake name role 2']);
        $request = $this->actingAs($user)->getJson(route('roles.index'));
        $request->assertJsonFragment(['name' => 'fake name role']);
        $request->assertJsonFragment(['name' => 'fake name role 2']);
        $request->assertStatus(200);
    }

    /** @test */
    public function guest_user_cannot_view_role_listing()
    {
        $request = $this->get(route('roles.index'));
        $request->assertRedirect(route('login'));
    }

    /**
    * @test
    */
    public function can_admin_viewrole_listing()
    {
        $user = $this->adminUser();
        $role = factory('App\Role')->create(['name' => 'fake name role']);
        $role2 = factory('App\Role')->create(['name' => 'fake name role 2']);
        $request = $this->actingAs($user)->get(route('roles.index'));
        $request->assertSee('fake name role');
        $request->assertSee('fake name role 2');
        $request->assertStatus(200);
    }
}
