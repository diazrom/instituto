<?php

namespace Tests\Feature\Admin\Roles;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminCreateRoleTest extends TestCase
{
    use RefreshDatabase;

    private function validParams($overrides = [])
    {
        return array_merge([
            'name' => 'fake name',
            'description' => 'fake description',
        ], $overrides);
    }

    /** @test */
    public function normal_user_cannot_create_a_role()
    {
        $user = $this->normalUser();
        $request = $this->actingAs($user)->post(route('roles.store'), $this->validParams());
        $request->assertStatus(401);
    }

    /** @test */
    public function can_admin_create_a_role()
    {
        $user = $this->adminUser();
        $request = $this->actingAs($user)->from(route('roles.index'))->post(route('roles.store'), $this->validParams());
        $request->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function name_is_required()
    {
        $user = $this->adminUser();
        $request = $this->actingAs($user)->post(route('roles.store'), [
            'description' => 'secret',
        ]);

        $request->assertStatus(302);
        $request->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function description_is_required()
    {
        $user = $this->adminUser();
        $request = $this->actingAs($user)->post(route('roles.store'), [
            'name' => 'name',
        ]);

        $request->assertStatus(302);
        $request->assertSessionHasErrors(['description']);
    }
}
