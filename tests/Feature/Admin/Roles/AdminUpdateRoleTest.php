<?php

namespace Tests\Feature\Admin\Roles;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminUpdateRoleTest extends TestCase
{
    use RefreshDatabase;

    private function validParams($overrides = [])
    {
        return array_merge([
            'name' => 'fake name',
            'description' => 'fake description',
        ], $overrides);
    }

    /** @test */
    public function can_admin_update_a_role()
    {
        $userAdmin = $this->adminUser();
        $role = factory('App\Role')->create(['name' => 'fake name role']);
        $request = $this->actingAs($userAdmin)->from(route('roles.index'))->patch(route('roles.update', $role), ['description' => 'fake3@mail.cl', 'permissions' => ['1']]);
        $this->assertDatabaseHas('roles', ['description' => 'fake3@mail.cl']);
        $request->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function name_is_required_sometimes()
    {
        $user = $this->adminUser();
        $role = factory('App\Role')->create(['name' => 'fake name role']);
        $request = $this->actingAs($user)->patch(route('roles.update', $role), [
            'description' => 'secret',
        ]);

        $this->assertDatabaseHas('roles', ['description' => 'secret']);
    }

    /** @test */
    public function description_is_required_sometimes()
    {
        $user = $this->adminUser();
        $role = factory('App\Role')->create(['name' => 'fake name role']);
        $request = $this->actingAs($user)->patch(route('roles.update', $role), [
            'description' => 'secret',
        ]);

        $this->assertDatabaseHas('roles', ['description' => 'secret']);
    }
}
