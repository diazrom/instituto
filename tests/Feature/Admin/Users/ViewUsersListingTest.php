<?php

namespace Tests\Feature\Admin\Users;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ViewUsersListingTest extends TestCase
{
    use RefreshDatabase;

    /**
    * @test
    */
    public function can_admin_view_user_listing_as_json_format()
    {
        $user = $this->adminUser();
        $user2 = factory('App\User')->create(['name' => 'user 1']);
        $user3 = factory('App\User')->create(['name' => 'user 2']);
        $request = $this->actingAs($user)->getJson(route('users.index'));
        $request->assertJsonFragment(['name' => 'user 1']);
        $request->assertJsonFragment(['name' => 'user 2']);
        $request->assertStatus(200);
    }

    /** @test */
    public function guest_user_cannot_view_user_listing()
    {
        $request = $this->get(route('users.index'));
        $request->assertRedirect(route('login'));
    }

    /**
    * @test
    */
    public function can_admin_view_user_listing()
    {
        $user = $this->adminUser();
        $user2 = factory('App\User')->create(['name' => 'user 1']);
        $user3 = factory('App\User')->create(['name' => 'user 2']);
        $request = $this->actingAs($user)->get(route('users.index'));
        $request->assertSee('user 1');
        $request->assertSee('user 2');
        $request->assertStatus(200);
    }
}
