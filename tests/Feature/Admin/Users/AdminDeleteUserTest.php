<?php

namespace Tests\Feature\Admin\Users;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminDeleteUserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_admin_delete_an_user()
    {
        $user = $this->adminUser();
        $deleteUser = factory(User::class)->create(['name' => 'deleted']);
        $request = $this->actingAs($user)->from(route('users.index'))->delete(route('users.destroy', $deleteUser));

        $this->assertDatabaseMissing('users', ['name' => 'deleted']);
        $request->assertRedirect(route('users.index'));
    }

    /** @test */
    public function normal_user_can_not_delete_an_user()
    {
        $user = $this->normalUser();
        $deleteUser = factory(User::class)->create(['name' => 'deleted']);
        $request = $this->actingAs($user)->delete(route('users.destroy', $deleteUser));

        $request->assertStatus(401);
    }

    /** @test */
    public function can_admin_delete_yourself()
    {
        $user = $this->adminUser();
        $request = $this->actingAs($user)->delete(route('users.destroy', $user));

        $request->assertStatus(403);
    }
}
