<?php

namespace Tests\Feature\Admin\Users;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Http\Middleware\UserHasPermission;

class AdminUpdateUserTest extends TestCase
{
    use RefreshDatabase;

    private function validParams($overrides = [])
    {
        return array_merge([
            'name' => 'fake name',
            'email' => 'fake@email.cl',
            'password' => 'secret',
            'active' => true,
        ], $overrides);
    }

    /** @test */
    public function user_can_not_update_other_user()
    {
        $this->withoutMiddleware(UserHasPermission::class); //For not make and give permission

        $userNormal = $this->normalUser();
        $user = $this->normalUser();
        $request = $this->actingAs($userNormal)->patch(route('users.update', $user), ['email' => 'faqweqwe3@mail.cl']);
        $request->assertStatus(403);
    }

    /** @test */
    public function can_user_update_yourself()
    {
        $this->withoutMiddleware(UserHasPermission::class); //For not make and give permission

        $user = $this->normalUser();
        $request = $this->actingAs($user)->from(route('users.index'))->patch(route('users.update', $user), ['email' => 'fake3@mail.cl', 'roles' => ['1']]);
        $request->assertRedirect(route('users.index'));
        $this->assertDatabaseHas('users', ['email' => 'fake3@mail.cl']);
    }

    /** @test */
    public function can_admin_update_an_user()
    {
        $this->withoutMiddleware(UserHasPermission::class); //For not make and give permission
        $userAdmin = $this->adminUser();
        $user = $this->normalUser();
        $request = $this->actingAs($userAdmin)->from(route('users.index'))->patch(route('users.update', $user), ['email' => 'fake3@mail.cl', 'roles' => ['1']]);
        $this->assertDatabaseHas('users', ['email' => 'fake3@mail.cl']);
        $request->assertRedirect(route('users.index'));
    }

    /** @test */
    public function email_must_be_valid()
    {
        $user = $this->adminUser();
        $user2 = $this->normalUser();
        $request = $this->actingAs($user)->patch(route('users.update', $user2), [
            'name' => 'name',
            'email' => 'fake-email',
            'password' => 'secret',
            'active' => true
        ]);

        $request->assertStatus(302);
        $request->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function active_must_be_valid()
    {
        $user = $this->adminUser();
        $user2 = $this->normalUser();
        $request = $this->actingAs($user)->patch(route('users.update', $user2), [
            'name' => 'name',
            'email' => 'fake@mail.cl',
            'password' => '123456',
            'active' => 'qwe'
        ]);

        $request->assertStatus(302);
        $request->assertSessionHasErrors(['active']);
    }
}
