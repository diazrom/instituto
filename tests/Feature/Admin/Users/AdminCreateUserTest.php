<?php

namespace Tests\Feature\Admin\Users;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminCreateUserTest extends TestCase
{
    use RefreshDatabase;

    private function validParams($overrides = [])
    {
        return array_merge([
            'name' => 'fake name',
            'email' => 'fake@email.cl',
            'password' => 'secret',
            'active' => '1',
        ], $overrides);
    }

    /** @test */
    public function normal_user_can_not_create_an_user()
    {
        $user = $this->normalUser();
        $request = $this->actingAs($user)->post(route('users.store'), $this->validParams());
        $request->assertStatus(401);
    }

    /** @test */
    public function can_admin_create_an_user()
    {
        $user = $this->adminUser();
        $request = $this->actingAs($user)->from(route('users.index'))->post(route('users.store'), $this->validParams());
        $request->assertRedirect(route('users.index'));
    }

    /** @test */
    public function name_is_required()
    {
        $user = $this->adminUser();
        $request = $this->actingAs($user)->post(route('users.store'), [
            'email' => 'fake@email.cl',
            'password' => 'secret',
            'active' => true
        ]);

        $request->assertStatus(302);
        $request->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function email_is_required()
    {
        $user = $this->adminUser();
        $request = $this->actingAs($user)->post(route('users.store'), [
            'name' => 'name',
            'password' => 'secret',
            'active' => true
        ]);

        $request->assertStatus(302);
        $request->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function email_must_be_valid()
    {
        $user = $this->adminUser();
        $request = $this->actingAs($user)->post(route('users.store'), [
            'name' => 'name',
            'email' => 'fake-email',
            'password' => 'secret',
            'active' => true
        ]);

        $request->assertStatus(302);
        $request->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function password_is_required()
    {
        $user = $this->adminUser();
        $request = $this->actingAs($user)->post(route('users.store'), [
            'name' => 'name',
            'email' => 'fake@mail.cl',
            'active' => true
        ]);

        $request->assertStatus(302);
        $request->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function password_must_be_valid()
    {
        $user = $this->adminUser();
        $request = $this->actingAs($user)->post(route('users.store'), [
            'name' => 'name',
            'email' => 'fake@mail.cl',
            'password' => '123',
            'active' => true
        ]);

        $request->assertStatus(302);
        $request->assertSessionHasErrors(['password']);
    }
}
